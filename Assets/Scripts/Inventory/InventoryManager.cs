﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG {
    public class InventoryManager : MonoBehaviour {
        public Weapon currentWeapon;
        PlayerState state;

        public void Initialize () {
            currentWeapon.weaponController.EndDamageColliders();
        }
    }

    [System.Serializable]
    public class Weapon {
        public List<Action> actions;
        public List<Action> thActions;
        public GameObject weaponModel;
        public WeaponController weaponController;
    }
}