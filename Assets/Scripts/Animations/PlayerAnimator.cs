﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG {
    public class PlayerAnimator : MonoBehaviour {
        Animator animator;
        PlayerState state;
        public float rmMult;
        public void Initialize (PlayerState st) {
            state = st;
            animator = st.anim;
        }

        void OnAnimatorMove () {
            if (state.canMove)
                return;

            state.rb.drag = 0;
            if (rmMult == 0)
                rmMult = 1f;
            Vector3 delta = animator.deltaPosition;
            delta.y = 0;
            Vector3 velo = (delta * rmMult) / state.delta;
            state.rb.velocity = velo;
        }

        public void LateTick () {

        }

        public void EnableDamageColliders(){
            state.inventoryManager.currentWeapon.weaponController.StartDamageColliders();
        }

        public void DisableDamageColliders(){
            state.inventoryManager.currentWeapon.weaponController.EndDamageColliders();
        }
    }
}