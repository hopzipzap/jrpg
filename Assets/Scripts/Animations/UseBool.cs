﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG {
    public class UseBool : StateMachineBehaviour {
        public string boolName;
        public bool status;
        public bool reset = true;

        override public void OnStateUpdate (Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
            animator.SetBool (boolName, status);
        }

        public override void OnStateExit (Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
            if (reset)
                animator.SetBool (boolName, !status);
        }
    }
}