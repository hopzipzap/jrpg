﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG {
    public class WeaponController : MonoBehaviour {
        public GameObject[] damageCollider;

        public void StartDamageColliders () {
            for (int i = 0; i < damageCollider.Length; i++) {
                damageCollider[i].SetActive(true);

            }
        }
        public void EndDamageColliders () {
            for (int i = 0; i < damageCollider.Length; i++) {
                damageCollider[i].SetActive(false);
                
            }
        }
    }

}