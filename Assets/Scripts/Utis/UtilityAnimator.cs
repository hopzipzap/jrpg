﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG {
    public class UtilityAnimator : MonoBehaviour {
        [Range (-1, 1)]
        public float inputV;
        [Range (-1, 1)]
        public float inputH;

        public bool playAnim;

        public string[] OHMoves;
        public string[] THMoves;

        public int weaponType;
        public bool useRootMotion;
        public bool useShield;
        public bool interact;
        public bool lockOn;
        Animator anim;

        void Start () {
            anim = GetComponent<Animator> ();
        }

        void Update () {

            useRootMotion = !anim.GetBool ("CanMove");
            anim.applyRootMotion = useRootMotion;
            interact = anim.GetBool ("Interacting");
            if (!lockOn) {
                inputH = 0;
                inputV = Mathf.Clamp01 (inputV);
            }
            anim.SetBool ("LockOn", lockOn);
            if (useRootMotion)
                return;
            if (useShield) {
                anim.Play ("Shield");
                useShield = false;
            }

            if (interact) {
                playAnim = false;
                inputV = Mathf.Clamp (inputV, 0, 0.5f);
            }
            anim.SetInteger ("WeaponType", weaponType);

            if (playAnim) {
                string currentAnim;

                if (weaponType == 1) {
                    int r = Random.Range (0, OHMoves.Length);
                    currentAnim = OHMoves[r];

                } else {
                    int r = Random.Range (0, THMoves.Length);
                    currentAnim = THMoves[r];
                }
                inputV = 0;
                anim.CrossFade (currentAnim, 0.2f);
                //anim.SetBool("CanMove",false);
                //useRootMotion = true;
                playAnim = false;
            }
            anim.SetFloat ("InputV", inputV);
            anim.SetFloat ("InputH", inputH);

        }
    }
}