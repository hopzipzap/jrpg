﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG {
    public class CameraController : MonoBehaviour {
        public bool lockOn;
        public float fSpeed = 9f;
        public float mSpeed = 2f;

        public Transform player;
        public Transform lockOnPos;
        public LockOnEnemy lockOnTarget;
        [HideInInspector]
        public Transform pivot;
        [HideInInspector]
        public Transform camPos;
        PlayerState state;
        float smooth = .1f;
        public float minAng = -35f;
        public float maxAng = 35f;
        float smoothX;
        float smoothY;
        float smoothXvelo;
        float smoothYvelo;
        public float lookAng;
        public float tiltAng;

        public void Initialize (PlayerState st) {
            state = st;
            player = st.transform;
            lockOn = false;
            camPos = Camera.main.transform;
            pivot = camPos.parent;

        }

        public void Tick (float d) {
            float h = Input.GetAxis ("Mouse X");
            float v = Input.GetAxis ("Mouse Y");

            float pSpeed = mSpeed;
            if (lockOnTarget != null) {
                if (lockOnPos == null) {
                    lockOnPos = lockOnTarget.GetTarget ();
                    state.enemyTransform = lockOnPos;
                }
            }
            PlayerFollow (d);
            CameraRotation (d, v, h, pSpeed);

        }

        void PlayerFollow (float d) {
            float speed = d * fSpeed;
            Vector3 playerPos = Vector3.Lerp (transform.position, player.position, speed);
            transform.position = playerPos;
        }

        void CameraRotation (float d, float v, float h, float pSpeed) {
            if (smooth > 0) {
                smoothX = Mathf.SmoothDamp (smoothX, h, ref smoothXvelo, smooth);
                smoothY = Mathf.SmoothDamp (smoothY, v, ref smoothYvelo, smooth);
            } else {
                smoothX = h;
                smoothY = v;
            }

            tiltAng -= smoothY * pSpeed;
            tiltAng = Mathf.Clamp (tiltAng, minAng, maxAng);
            pivot.localRotation = Quaternion.Euler (tiltAng, 0, 0);

            if (lockOn && lockOnTarget != null) {
                Vector3 enemyDir = lockOnPos.position - transform.position;
                enemyDir.Normalize ();
                //enemyDir.y = 0;

                if (enemyDir == Vector3.zero)
                    enemyDir = transform.forward;
                Quaternion enemyRot = Quaternion.LookRotation (enemyDir);
                transform.rotation = Quaternion.Slerp (transform.rotation, enemyRot, d * 9f);
                lookAng = transform.eulerAngles.y;
                return;
            }

            lookAng += smoothX * pSpeed;
            transform.rotation = Quaternion.Euler (0, lookAng, 0);

        }

        public static CameraController singleton;

        void Awake () {
            singleton = this;
        }
    }
}