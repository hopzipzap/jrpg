﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG {
    public class PlayerState : MonoBehaviour {
        #region Variables
        [Header ("Player")]
        public GameObject currPlayer;

        [Header ("Input")]
        public float inputV;
        public float inputH;
        public float mCount;
        public Vector3 mDirection;
        public bool rt, rtb, lt, lb;
        public bool itemInput;
        public bool rollInput;

        [Header ("Move Parameters")]
        public float mSpeed = 2f;
        public float rSpeed = 3.5f;
        public float delta;
        public float rotSpeed = 8f;
        public float groundDist = 0.5f;
        public float rollSpeed = 1f;

        [Header ("State")]
        public bool grounded;
        public bool run;
        public bool lockOn;
        public bool onAction;
        public bool canMove;
        public bool useItem;
        [Header ("Other")]
        public LockOnEnemy enemy;
        public Transform enemyTransform;
        #region Components

        [HideInInspector]
        public Animator anim;
        [HideInInspector]
        public Rigidbody rb;
        [HideInInspector]
        public PlayerAnimator animPlayer;
        [HideInInspector]
        public ActionController actionController;
        [HideInInspector]
        public InventoryManager inventoryManager;
        #endregion;
        [HideInInspector]
        public LayerMask ignoreLayers;
        #endregion

        float actionTimer;

        public void Initialize () {
            SetAnimator ();
            rb = GetComponent<Rigidbody> ();
            rb.angularDrag = 999;
            rb.drag = 4;
            rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
            inventoryManager = GetComponent<InventoryManager> ();
            actionController = GetComponent<ActionController> ();
            inventoryManager.Initialize ();
            actionController.Initialize (this);
            animPlayer = currPlayer.AddComponent<PlayerAnimator> ();
            animPlayer.Initialize (this);
            gameObject.layer = 8;
            SetWeaponType ();
            ignoreLayers = ~(1 << 9);
            anim.SetBool ("Grounded", true);
        }

        void SetAnimator () {
            if (currPlayer == null) {
                anim = GetComponentInChildren<Animator> ();
                if (anim == null)
                    print ("no player assigned");
                else
                    currPlayer = anim.gameObject;
            }
            if (anim == null)
                anim = currPlayer.GetComponent<Animator> ();
        }

        public void FixedTick (float d) {
            delta = d;

            useItem = anim.GetBool ("Interacting");
            DetectAction ();
            OnAction ();
            anim.applyRootMotion = true;
            inventoryManager.currentWeapon.weaponModel.SetActive (!useItem);
            if (onAction) {
                anim.applyRootMotion = false;
                actionTimer += delta;
                if (actionTimer > 0.3f) {
                    onAction = false;
                    actionTimer = 0;
                } else
                    return;
            }
            canMove = anim.GetBool ("CanMove");
            if (!canMove)
                return;
            animPlayer.rmMult = 1f;
            DodgeMechanics ();
            anim.applyRootMotion = false;
            if (mCount > 0 || !grounded)
                rb.drag = 0;
            else
                rb.drag = 4;
            float pSpeed = mSpeed;
            if (run)
                pSpeed = rSpeed;
            if (grounded)
                rb.velocity = mDirection * (pSpeed * mCount);
            Vector3 pDirection = (!lockOn || (lockOn && run && !onAction)) ? mDirection :
                (enemyTransform != null) ?
                enemyTransform.transform.position - transform.position :
                mDirection;
            pDirection.y = 0;
            if (pDirection == Vector3.zero)
                pDirection = transform.forward;
            Quaternion pRot = Quaternion.LookRotation (pDirection);
            Quaternion pRotation = Quaternion.Slerp (transform.rotation, pRot, delta * mCount * rotSpeed);
            transform.rotation = pRotation;
            anim.SetBool ("LockOn", lockOn);
            if (!lockOn)
                MovementAnimation ();
            else
                LockOnAnimation (mDirection);
        }
        public void DetectAction () {
            if (!canMove)
                return;
            if (!itemInput)
                return;
            ItemAction slot = actionController.consumable;
            string currentAnim = slot.currentAnim;
            if (string.IsNullOrEmpty (currentAnim))
                return;
            useItem = true;
            anim.Play (currentAnim);

        }

        public void OnAction () {
            if (canMove == false || itemInput)
                return;
            if (rtb == false && rt == false && lt == false && lb == false)
                return;
            string currAnim = null;
            Action slot = actionController.GetActionSlot (this);
            if (slot == null)
                return;
            currAnim = slot.currentAnim;
            if (string.IsNullOrEmpty (currAnim))
                return;

            canMove = false;
            onAction = true;
            anim.CrossFade (currAnim, 0.2f);
            //rb.velocity = Vector3.zero;

        }
        public void Tick (float d) {
            delta = d;
            grounded = Grounded ();
            anim.SetBool ("Grounded", grounded);
        }

        void DodgeMechanics () {
            float v = inputV;;
            float h = inputH;
            if (!rollInput || useItem)
                return;
            v = (mCount > 0.3f) ? 1 : 0;
            h = 0;

            animPlayer.rmMult = rollSpeed;
            if (v != 0) {
                if (mDirection == Vector3.zero)
                    mDirection = transform.forward;
                Quaternion plRot = Quaternion.LookRotation (mDirection);
                transform.rotation = plRot;
            }
            anim.SetFloat ("InputV", v);
            anim.SetFloat ("InputH", h);
            canMove = false;
            onAction = true;
            anim.CrossFade ("Dodges", 0.2f);
        }

        void MovementAnimation () {
            anim.SetBool ("Run", run);
            anim.SetFloat ("InputV", mCount, 0.4f, delta);
        }

        void LockOnAnimation (Vector3 mDir) {
            Vector3 relDir = transform.InverseTransformDirection (mDir);
            float h = relDir.x;
            float v = relDir.z;

            anim.SetBool ("Run", run);
            anim.SetFloat ("InputV", v, 0.4f, delta);
            anim.SetFloat ("InputH", h, 0.4f, delta);
        }

        public bool Grounded () {
            bool ret = false;
            Vector3 origin = transform.position + (Vector3.up * groundDist);
            Vector3 direction = -Vector3.up;
            float distance = groundDist + 0.3f;
            RaycastHit hit;
            Debug.DrawRay (origin, direction * distance);
            if (Physics.Raycast (origin, direction, out hit, distance, ignoreLayers)) {
                ret = true;
                Vector3 playerPosition = hit.point;
                transform.position = playerPosition;
            }
            return ret;
        }

        public void GetWeaponType () {
            int weaponType;
            weaponType = anim.GetInteger ("WeaponType");
            if (weaponType == 0) {
                actionController.UpdateActionWeaponTypeTH ();
                anim.SetInteger ("WeaponType", 1);
            }
            if (weaponType == 1) {
                actionController.UpdateActionWeaponTypeOH ();
                anim.SetInteger ("WeaponType", 0);
            }
        }

        public void SetWeaponType () {
            int weaponType;
            weaponType = anim.GetInteger ("WeaponType");
            if (weaponType == 0) {
                actionController.UpdateActionWeaponTypeOH ();
            }
            if (weaponType == 1) {
                actionController.UpdateActionWeaponTypeTH ();
            }
        }
    }
}