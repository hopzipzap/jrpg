﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG {
    public class ActionController : MonoBehaviour {
        public List<Action> slots = new List<Action> ();
        public ItemAction consumable;

        PlayerState state;

        public void Initialize (PlayerState st) {
            state = st;
        }

        public void UpdateActionWeaponTypeOH () {
            EmptyAction ();
            Weapon weapon = state.inventoryManager.currentWeapon;

            for (int i = 0; i < weapon.actions.Count; i++) {
                Action a = GetAction (weapon.actions[i].input);
                a.currentAnim = weapon.actions[i].currentAnim;
            }
        }

        public void UpdateActionWeaponTypeTH () {
            EmptyAction ();
            Weapon weapon = state.inventoryManager.currentWeapon;
            
            for (int i = 0; i < weapon.thActions.Count; i++) {
                Action a = GetAction (weapon.thActions[i].input);
                a.currentAnim = weapon.thActions[i].currentAnim;
            }
        }

        void EmptyAction () {
            for (int i = 0; i < 4; i++) {
                Action a = GetAction ((ActionInput) i);
                a.currentAnim = null;
            }
        }

        ActionController () {
            for (int i = 0; i < 4; i++) {
                Action a = new Action ();
                a.input = (ActionInput) i;
                slots.Add (a);
            }
        }

        public Action GetActionSlot (PlayerState st) {
            ActionInput aInput = GetActionInput (st);
            return GetAction (aInput);
        }

        Action GetAction (ActionInput input) {
            for (int i = 0; i < slots.Count; i++) {
                if (slots[i].input == input)
                    return slots[i];
            }

            return null;
        }

        public ActionInput GetActionInput (PlayerState st) {
            if (st.rtb)
                return ActionInput.rb;
            if (st.rt)
                return ActionInput.rt;
            if (st.lb)
                return ActionInput.lb;
            if (st.lt)
                return ActionInput.lt;
            return ActionInput.rb;
        }
    }

    public enum ActionInput {
        rb,
        lb,
        rt,
        lt,
        x,
    }

    [System.Serializable]
    public class Action {
        public ActionInput input;
        public string currentAnim;
    }

    [System.Serializable]
    public class ItemAction
    {
        public string currentAnim;
        public string itemId;
    }
}