﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace RPG {
    public class PlayerController : MonoBehaviour {
        #region Variables
        float inputV;
        float inputH;
        float delta;
        #region InputControls
        bool run;
        bool aInput;
        bool yInput;
        bool xInput;
        bool rtMouse;
        bool ltMouse;
        bool rInput;
        bool lInput;
        float lAxis;
        float rAxis;
        float bTimer;
        float rtTimer;
        float ltTimer;
        bool lockOnKeyDown;
        bool lockOnKeyUp;
        #endregion
        PlayerState state;
        CameraController cameraController;
        #endregion
        void Start () {
            state = GetComponent<PlayerState> ();

            state.Initialize ();
            Cursor.lockState = CursorLockMode.Locked;
            cameraController = CameraController.singleton;
            cameraController.Initialize (state);
        }

        void FixedUpdate () {
            delta = Time.fixedDeltaTime;
            rtMouse = false;
            yInput = false;
            ltMouse = false;
            run = false;
            OnInput ();
            StateUpdate ();
            state.FixedTick (delta);
            cameraController.Tick (delta);

        }

        void Update () {
            delta = Time.deltaTime;
            state.Tick (delta);
            InputStateReset ();
        }

        void OnInput () {
            inputV = Input.GetAxis ("Vertical");
            inputH = Input.GetAxis ("Horizontal");
            run = Input.GetButton ("RunInput");

            aInput = Input.GetButton ("A");
            yInput = Input.GetButtonDown ("Y");
            xInput = Input.GetButton ("X");
            rtMouse = Input.GetButton ("RB");
            ltMouse = Input.GetButton ("LB");
            rInput = Input.GetButton ("RT");
            rAxis = Input.GetAxis ("RT");
            if (rAxis != 0)
                rInput = true;
            lInput = Input.GetButton ("LT");
            lAxis = Input.GetAxis ("LT");
            if (lAxis != 0)
                lInput = true;

            lockOnKeyDown = Input.GetButtonUp ("Lock");
            if (run)
                bTimer += delta;
        }

        void StateUpdate () {
            state.inputH = inputH;
            state.inputV = inputV;
        
            Vector3 v = inputV * cameraController.transform.forward;
            Vector3 h = inputH * cameraController.transform.right;
            state.mDirection = (v + h).normalized;
            float m = Mathf.Abs (inputH) + Mathf.Abs (inputV);
            state.mCount = Mathf.Clamp01 (m);
            if (xInput)
                run = false;
            if (run && bTimer > 0.5f)
                state.run = (state.mCount > 0);
            if (run == false && bTimer > 0 && bTimer < 0.5f)
                state.rollInput = true;
            state.itemInput = xInput;
            state.rt = rInput;
            state.lt = lInput;
            state.rtb = rtMouse;
            state.lb = ltMouse;
            if (yInput) {
                state.GetWeaponType ();
            }
            if (lockOnKeyDown) {
                state.lockOn = !state.lockOn;
                if (state.enemy == null)
                    state.lockOn = false;
                cameraController.lockOnTarget = state.enemy;
                state.enemyTransform = cameraController.lockOnPos;
                cameraController.lockOn = state.lockOn;
            }

        }

        void InputStateReset () {
            if (run == false)
                bTimer = 0f;
            if (state.rollInput)
                state.rollInput = false;
            if (state.run)
                state.run = false;
        }
    }
}