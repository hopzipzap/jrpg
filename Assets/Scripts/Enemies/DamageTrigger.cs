﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG {
    public class DamageTrigger : MonoBehaviour {

        void OnTriggerEnter(Collider other) {
            EnemyState eState = other.transform.root.GetComponentInParent<EnemyState>();
        
            if (eState == null)
                return;

            eState.Damage(5f);
        }
    }
}