﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG {
    public class EnemyState : MonoBehaviour {
        public float hp;
        Animator animator;
        LockOnEnemy enemy;
        public bool isInvincible;

        void Start () {
            animator = GetComponentInChildren<Animator> ();
            enemy = GetComponent<LockOnEnemy> ();
            enemy.Initialize (animator);
        }

        private void Update () {
            if (isInvincible)
                isInvincible = !animator.GetBool ("CanMove");
        }

        public void Damage (float v) {
            if (isInvincible)
                return;
            hp -= v;
            isInvincible = true;

            animator.Play ("impact1");
        }
    }
}