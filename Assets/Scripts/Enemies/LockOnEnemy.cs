﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockOnEnemy : MonoBehaviour {
    public int index;
    public List<HumanBodyBones> hBones = new List<HumanBodyBones> ();
    public List<Transform> enemies = new List<Transform> ();
    Animator animator;

    public void Initialize(Animator a) {
        animator = a;
        animator = GetComponentInChildren <Animator> ();
        if (!animator.isHuman)
            return;
        for (int i = 0; i < hBones.Count; i++) {
            enemies.Add (animator.GetBoneTransform (hBones[i]));
        }
    }

    public Transform GetTarget (bool neg = false) {
        if (enemies.Count == 0)
            return transform;
        int enemyIndex = index;
        if (!neg) {
            if (index < enemies.Count - 1) {
                index++;
            } else {
                index = 0;
                enemyIndex = 0;
            }
        } else {
            if (index < 0) {
                index = enemies.Count - 1;
                enemyIndex = enemies.Count - 1;;
            } else {
                index--;
            }
        }
        index = Mathf.Clamp (index, 0, enemies.Count);
        return enemies[enemyIndex];
    }
}